import { cloneDeep, omit } from 'lodash-es';
import { Component, VueConstructor, VNodeData } from 'vue';

function pascalCase(text: string) {
  return text.replace(/(^\w|-\w)/g, (s) => s.replace(/-/, '').toUpperCase());
}

function getComponent(
  type: any,
  vue: VueConstructor
): Parameters<VueConstructor['extend']>[0] | void {
  if (typeof type === 'string') {
    const component: any = vue.component(type) || vue.component(pascalCase(type));
    return component && component.options;
  }

  if (typeof type.render === 'function') {
    return type;
  }
}

export function getComponentProps(type: string | Component, vue: VueConstructor): string[] {
  const component = getComponent(type, vue);

  if (!component || !component.props) return [];

  return Object.keys(component.props);
}

/**
 * parse raw props to VNodeData
 */
export function parseProps(o?: Record<string, any>, props: string[] = []): VNodeData {
  if (!o) return {};
  const vnodeOpts = ['class', 'style', 'ref', 'refInFor', 'on', 'nativeOn', 'directives'];
  return Object.keys(o).reduce(
    (map: any, name) => {
      if (vnodeOpts.includes(name)) {
        map[name] = o[name];
        return map;
      }

      if (props.includes(name)) {
        map.props[name] = o[name];
      } else {
        map.attrs[name] = o[name];
      }

      return map;
    },
    { props: {}, attrs: {} }
  );
}

export function log(fieldKey: string, prop: string, before: any, after: any) {
  if (before === after || after === undefined) return;
  console.info(
    `[mutate]`,
    `${fieldKey}.${prop} ${JSON.stringify(before)} => ${JSON.stringify(after)}`
  );
}

export { cloneDeep, omit };
