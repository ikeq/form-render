import Vue, { Component, CreateElement, CSSProperties, PropType, VNode } from 'vue';
import { log, cloneDeep, omit, getComponentProps, parseProps } from './utils';

const mutableProps = ['active', 'value', 'props', 'visible'] as const;
const xProps = [
  'name',
  'key',
  'type',
  'props',
  'slots',
  'value',
  'visible',
  'active',
  'deps',
  'transform',
  'class',
  'style',
] as const;

type DepHandler<Value = any> = (
  value: Readonly<Value>,
  model: Readonly<any>,
  self: Readonly<Field<Value>>,
  vm: Readonly<any>
) => Pick<Field<Value>, typeof mutableProps[number]>;

export interface Field<Value = any, Slots = Record<string, () => VNode>> {
  name: string;
  key?: string;
  type?: string | Component;
  props?: Record<string, any>;
  slots?: Slots;
  value?: Value;
  visible?: boolean;
  active?: boolean;
  deps?: Record<string, DepHandler<Value>>;
  style: CSSProperties;
  class: any;
  transform?: {
    in?: (model?: Record<string, any>) => any;
    out?: (value?: any) => Record<string, any>;
  };
  [prop: string]: any;
}

type MutationInfo = Array<ReturnType<DepHandler>>;

function groupDeps(fields: Field[]): Record<string, [string, DepHandler][]> {
  const deps = {} as any;

  fields.forEach((field) => {
    if (field.deps) {
      for (const dependedKey in field.deps) {
        if (!deps[dependedKey]) {
          deps[dependedKey] = [] as any;
        }
        deps[dependedKey].push([field.key as string, field.deps[dependedKey]]);
      }
    }
  });

  return deps;
}

function mutate(value: any, target: Field, handler: DepHandler, model: any, vm: any) {
  const mutated = handler(value, model, target, vm);
  const targetKey = target.key || target.name;

  if (!mutated) return [];

  const mutateState: MutationInfo = [];

  function snapshot() {
    mutateState.push({
      value: model[targetKey],
      active: target.active,
      visible: target.visible,
      props: target.props,
    });
    return mutateState;
  }

  snapshot();

  if (mutated.active !== target.active) {
    // auto reset value after being deactived (or initializing)
    if (mutated.active === false) {
      if (model[targetKey] !== undefined) {
        model[targetKey] = undefined;
      }
      target.active = false;
    }
    // auto fill default value after being actived
    else if (mutated.active === true) {
      const nextValue = mutated.value ?? target.value;
      if (nextValue !== undefined && model[targetKey] !== nextValue) {
        model[targetKey] = nextValue;
      }
      target.active = true;
    }
  }

  delete mutated.active;
  if (target.active === false) return snapshot();

  for (const k in mutated) {
    const prop = k as typeof mutableProps[number];
    if (!mutableProps.includes(prop)) return snapshot();
    if (prop === 'props') {
      if (mutated.props) {
        target.props = { ...target.props, ...mutated.props };
      }
    } else if (prop === 'value') {
      model[targetKey] = mutated.value;
    } else if (target[prop] !== mutated[prop]) {
      Object.assign(target, { [prop]: mutated[prop] });
    }
  }
  return snapshot();
}

function normalizeSlots(map: Record<string, any> = {}, h: CreateElement) {
  const ret = {} as Record<string, () => VNode>;
  for (const slot in map) {
    const def = map[slot];
    if (!def) continue;
    const comp =
      typeof def === 'function'
        ? (...args: any[]) => def(h, ...args)
        : typeof def.render === 'function'
        ? () => h(def)
        : () => def;

    ret[slot] = comp;
  }
  return ret;
}

interface CreateOptions<Form = Vue> {
  name?: string;
  form?: {
    component?: string | Component;
    props?: Record<string, any>;
  };
  formItem?: {
    component?: string | Component;
    props?: Record<string, any>;
    mutableProps?: string[];
  };
  api?: {
    [name: string]: (form: Form, ctx: { value: any }, ...args: any[]) => unknown;
  };
}

export const FORM_REF = 'form-render';

export function create<Form>({
  form = {},
  formItem = {},
  name = 'form-render',
  api = {},
}: CreateOptions<Form>) {
  const formComponent = form.component || 'div';
  const formItemComponent = formItem.component || 'div';
  const formProps = form.props || {};
  const formItemProps = formItem.props || {};

  return Vue.extend({
    name,
    props: {
      ...formProps,
      items: Array as PropType<
        Field<any, Record<string, Component | ((h?: CreateElement) => Component)>>[]
      >,
      defaultValue: Object,
    },
    provide() {
      return {
        [FORM_REF]: this,
      };
    },
    data() {
      const fields = {} as Record<string, Field>;
      const fieldKeys = [] as string[];
      const rendered = (this.items as Field[]).map((item) => {
        const cloned = cloneDeep(item);

        if (item.type) {
          // store component props for later use
          cloned.componentProps = getComponentProps(item.type, Vue);
        }

        cloned.key = cloned.key || cloned.name;
        cloned.props = cloned.props || {};
        cloned.slots = normalizeSlots(cloned.slots, this.$createElement);

        if (cloned.key) {
          fields[cloned.key] = cloned;
          fieldKeys.push(cloned.key);
        }

        return cloned;
      });

      const { defaultValue = {} } = this;
      const model = fieldKeys.reduce((map, key) => {
        const field = fields[key];
        if (field.active === false) return map;
        return {
          ...map,
          [key]: defaultValue[key] !== undefined ? defaultValue[key] : fields[key].value,
        };
      }, {}) as any;
      const deps = groupDeps(fieldKeys.map((i) => fields[i]));

      for (const key in deps) {
        const callback = (value: any) => {
          deps[key].forEach((i) => {
            const [targetKey, handler] = i;
            const target = fields[targetKey];

            const [m0, m1] = mutate(value, target, handler, model, this);
            if (m0 && process.env.NODE_ENV === 'development') {
              log(targetKey, 'active', m0.active, m1.active);
              log(targetKey, 'value', m0.value, m1.value);
              log(targetKey, 'props', m0.props, m1.props);
            }
          });
        };

        callback(defaultValue[key]);

        // register deps
        this.$nextTick(() => {
          this.$watch(`model.${key}`, callback);
        });
      }

      return {
        fields,
        rendered,
        model,
      };
    },
    methods: {
      ...Object.keys(api).reduce((ret, apiName) => {
        return {
          ...ret,
          [apiName](this: Vue, ...args: any[]) {
            const vm = this as any;
            const fn = api[apiName];
            const ctx = {
              get value() {
                return cloneDeep(vm.model);
              },
            };
            return fn(this.$refs.form as any, ctx, ...args);
          },
        };
      }, {}),
    },
    render(this: any, h) {
      const { model, fields, rendered } = this;

      return h(
        formComponent,
        {
          props: {
            ...omit(this.$props, ['items', 'defaultValue']),
            model,
          },
          ref: 'form',
        },
        rendered.map((item: Field) => {
          const key = item.key || item.name;
          const isField = !!fields[key];
          const vnodeData = parseProps(item.props, item.componentProps);
          const { slots } = item;

          // static components
          if (!isField && item.type) {
            return h(item.type, { ...vnodeData, scopedSlots: slots });
          }
          if (!item.type || item.active === false) return null;

          return h(
            formItemComponent,
            {
              props: {
                ...formItemProps,
                ...omit(item, xProps),
                prop: key,
              },
              style: {
                ...item.style,
                display: item.visible !== false ? '' : 'none',
              },
              class: item.class,
              key,
            },
            [
              h(item.type, {
                ...vnodeData,
                scopedSlots: slots,
                ref: `item-${key}`,
                model: {
                  value: model[key],
                  callback: (v: any) => {
                    model[key] = v;
                    this.$nextTick(() => {
                      this.$emit('change', cloneDeep(model));
                    });
                  },
                  expression: 'model[key]',
                },
              } as any),
            ]
          );
        })
      );
    },
  });
}

export default { create };
