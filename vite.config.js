import { defineConfig } from 'vite';
import { join } from 'path';
import { createVuePlugin as vue } from 'vite-plugin-vue2';
import markdownToJs, { transformImports, transformVue } from 'vite-plugin-markdown-to-js';
import replace from 'vite-plugin-filter-replace';

const template = () => {
  return `
<form-render ref="form" :items="schema" />
<εrow>
<εcol :push="6"><εbutton type="primary" @click="submit">Submit</εbutton></εcol>
</εrow>`;
};

const script = (code, imports) => {
  if (imports?.length) {
    return `export default { components: { ${imports} } };`;
  }
  return `
  ${code}

  export default {
    computed: {
      schema() {
        return schema;
      },
    },
    methods: {
      submit() {
        this.$refs.form
          .submit()
          .then((params) => {
            alert(JSON.stringify(params, null, 2));
          })
          .catch(alert);
      },
    },
  };
  `;
};

export default defineConfig({
  plugins: [
    vue({
      include: [/\.md$/],
    }),
    markdownToJs({
      transforms: [
        transformImports({ defaultPrefix: 'doc', base: './' }),
        transformVue({
          importsAsComponents: true,
          renders: {
            template(code, _, env) {
              return env.matter.root ? '' : template(code);
            },
            script(code, { imports }) {
              return script(code, imports);
            },
          },
        }),
      ],
      render(output) {
        let html = output.html.replace(
          /<!-- langvue -->([\s\S]*?)<!-- endlangvue -->/,
          '<!-- detailsopen --><div class="v-code">$1</div>'
        );
        if (!output.matter.root) {
          html = `<div class="v-case">${html.replace('<!-- detailsopen -->', '</div>')}`;
          return [
            `<template>${`<v-page slug="${output.matter.slug}" title="${output.matter.title}">${html}</v-page>`}</template>`,
            output.script,
          ].join('\n');
        }
        return [`<template><div>${html}</div></template>`, output.script].join('\n');
      },
      markedOptions: {},
    }),
    replace(
      [
        {
          filter: /\.md/,
          replace: [
            { from: /ε(button|input|textarea|icon|tooltip|switch|checkbox|row|col)/g, to: 'a-$1' },
          ],
        },
      ],
      { enforce: 'pre' }
    ),
  ],
  resolve: {
    alias: {
      '@tencent/form-render': join(__dirname, './lib'),
    },
  },
  server: {
    port: 3001,
  },
});
