---
title: 最基本的表单项渲染
slug: basic
---

```vue
<script>
const schema = [
  {
    name: 'firstName',
    label: 'First name',
    type: 'εinput',
  },
  {
    name: 'lastName',
    label: 'Last name',
    type: 'εinput',
  },
  {
    name: 'bio',
    label: 'Bio',
    type: 'εtextarea',
  },
];
</script>
```
