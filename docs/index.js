import Vue from 'vue';
import VueRouter from 'vue-router';

import antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';

import { create } from '../lib';
import docs from './README.md';
import './style.less';

const FormRender = create({
  form: {
    component: 'a-form-model',
    props: {
      labelCol: {
        type: Object,
        default: () => ({ span: 6 }),
      },
      wrapperCol: {
        type: Object,
        default: () => ({ span: 18 }),
      },
    },
  },
  formItem: {
    component: 'a-form-model-item',
    props: {
      hasFeedback: true
    }
  },
  api: {
    submit(form, ctx) {
      return new Promise((resolve) => {
        form.validate((valid) => valid && resolve(ctx.value));
      });
    },
    reset(form) {
      form.reset();
    },
  },
});

Vue.use(antd);
Vue.use(VueRouter);
Vue.component('FormRender', FormRender);
Vue.component('v-page', {
  props: {
    slug: String,
    title: String,
  },
  created() {
    const vnodes = this.$slots.default;
    const route = {
      name: this.slug,
      path: `/${this.slug}`,
      meta: {
        slug: this.slug,
        title: this.title,
      },
      component: {
        props: {
          layout: {},
        },
        render(h) {
          return h(this.layout, {}, vnodes);
        },
      },
    };
    this.$router.addRoute(route);
  },
  render() {
    return null;
  },
});

new Vue({
  router: new VueRouter({
    mode: 'history',
    linkActiveClass: 'actived',
    linkExactActiveClass: 'actived',
    routes: [
      {
        path: '/',
        redirect: '/basic',
      },
    ],
  }),
  render(h) {
    const routes = this.$router.getRoutes().filter((i) => i.meta?.slug);
    const layout = {
      render(h) {
        return h('div', {}, [
          h('aside', { class: 'v-sider' }, [
            h('header', { class: 'v-sider__header' }, [
              h('h1', ['@tencent/form-render']),
              h('div', { props: { to: '/' } }, ['A simple but powerful form render for vue2']),
            ]),
            h(
              'nav',
              { class: 'v-nav' },
              routes.map((route) => {
                return h('router-link', { class: 'v-nav__item', props: { to: route.meta.slug } }, [
                  route.meta.title,
                ]);
              })
            ),
          ]),
          this.$slots.default,
        ]);
      },
    };
    return h('div', {}, [
      h(docs),
      h('router-view', {
        class: 'v-container',
        props: { layout },
      }),
    ]);
  },
}).$mount('#app');
