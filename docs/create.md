---
title: create (建设中)
slug: create
---

```vue
<script>
import { create } from '@tencent/form-render';

const schema = [];
const FormRender = create({
  form: {
    component: 'a-form-model',
    props: {
      labelCol: {
        type: Object,
        default: () => ({ span: 6 }),
      },
      wrapperCol: {
        type: Object,
        default: () => ({ span: 18 }),
      },
    },
  },
  formItem: {
    component: 'a-form-model-item',
    props: {
      hasFeedback: true
    }
  },
  api: {
    submit(form, ctx) {
      return new Promise((resolve) => {
        form.validate((valid) => valid && resolve(ctx.value));
      });
    },
    reset(form) {
      form.reset();
    },
  },
});
</script>
```
