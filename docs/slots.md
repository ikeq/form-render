---
title: 表单项 slots
slug: slots
---

```vue
<script>
const schema = [
  {
    name: 'amount',
    label: 'Amount',
    type: 'εinput',
    slots: {
      prefix: '￥',
      suffix: 'RMB',
    },
  },
  {
    name: 'userName',
    label: 'User Name',
    type: 'εinput',
    slots: {
      prefix(h) {
        return h('εicon', { props: { type: 'user' } });
      },
      suffix(h) {
        return h('εtooltip', { props: { title: 'Extra information' } }, [
          h('εicon', { props: { type: 'info-circle' } }),
        ]);
      },
    },
  },
  {
    name: 'website',
    label: 'Website',
    type: 'εinput',
    slots: {
      suffix: {
        render(h) {
          return h('mark', '.com');
        },
      },
    },
  },
  {
    name: 'email',
    label(h) {
      return h('εtooltip', { props: { title: 'Extra information' } }, [
        h('span', ['Email ']),
        h('εicon', { props: { type: 'info-circle' } }),
      ]);
    },
    type: 'εinput',
  },
];
</script>
```
