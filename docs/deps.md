---
title: 联动
slug: deps
---

通过 Field.deps 设置联动，默认可联动属性为：

```ts
interface Field<Value = any> {
  /** 表单项的值 */
  value?: Value;
  /** 表单项是否可见 (css display 属性) */
  visible?: boolean;
  /** 表单项是否启用 */
  active?: boolean;
}
```

```vue
<script>
const schema = [
  {
    name: 'age',
    label: 'Age',
    value: 17,
    type: 'εinput-number',
    deps: {
      marriedAge: (marriedAge) => ({
        props: { min: marriedAge },
      }),
    },
  },
  {
    name: 'married',
    label: 'Married',
    type: 'εswitch',
    value: false,
    deps: {
      age: (age) => ({ active: age > 17 }),
    },
  },
  {
    name: 'marriedAge',
    label: 'Married Age',
    type: 'εinput-number',
    props: {
      min: 18,
    },
    deps: {
      married: (married, model, self) => ({
        active: !!married,
        value: model.age,
      }),
    },
  },
  {
    name: 'resetName',
    label: 'Reset Name',
    type: 'εcheckbox',
    props: {
      min: 18,
    },
  },
  {
    name: 'name',
    label: 'Name',
    type: 'εinput',
    deps: {
      resetName: () => ({ value: undefined }),
    },
  },
];
</script>
```
