---
title: 校验 rules
slug: rules
---

```vue
<script>
const schema = [
  {
    name: 'firstName',
    label: 'First name',
    type: 'εinput',
    rules: [
      { required: true, message: 'First name is required' },
      {
        validator(rule, value) {
          return new Promise((resolve, reject) => {
            setTimeout(() => {
              if (value.toLowerCase() === 'bob') return reject('Bob is taken');
              resolve();
            }, 500);
          });
        },
      },
    ],
  },
  {
    name: 'lastName',
    label: 'Last name',
    type: 'εinput',
    rules: {
      message: 'Last name is required',
      required: true,
    },
  },
  {
    name: 'bio',
    label: 'Bio',
    type: 'εtextarea',
  },
];
</script>
```
