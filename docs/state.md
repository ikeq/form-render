---
title: 表单项状态字段
slug: state
---

```ts
interface Field<Value = any> {
  /** 表单项的值 */
  value?: Value;
  /** 表单项是否可见 (css display 属性) */
  visible?: boolean;
  /** 表单项是否启用 (禁用时，值不会被提交) */
  active?: boolean;
}
```

```vue
<script>
const schema = [
  {
    name: 'firstName',
    label: 'First name',
    type: 'εinput',
    value: 'Jhon',
  },
  {
    name: 'lastName',
    label: 'Last name',
    type: 'εinput',
    value: 'Wick',
    visible: false,
  },
  {
    name: 'bio',
    label: 'Bio',
    type: 'εtextarea',
    value: 'Awesome guy',
    active: false,
  },
];
</script>
```
