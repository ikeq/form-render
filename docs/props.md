---
title: 表单项 props
slug: props
---

```vue
<script>
const schema = [
  {
    name: 'firstName',
    label: 'First name',
    type: 'εinput',
    props: {
      class: 'my-input',
      style: 'border: 1px solid red',
      maxLength: 10,
    },
  },
];
</script>
```
