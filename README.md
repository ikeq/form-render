# vue-form-render

```vue
<template>
  <div>
    <form-render
      :items="schema"
      :default-value="{ name: '张三' }"
      ref="form"
      label-width="120px"
    ></form-render>
    <el-button @click="submit" type="primary">确定</el-button>
    <el-button @click="$refs.form.reset()">重置</el-button>
  </div>
</template>

<script>
import { create } from '@tencent/vue-form-render';

const FormRender = create({
  name: 'el-form-render',
  form: {
    component: 'el-form',
    props: {
      labelWidth: {
        type: Object,
        default: () => ({ span: 6 }),
      },
    },
  },
  formItem: {
    component: 'el-form-item',
    mutableProps: ['label'], // 设置 label 可以被联动
  },
  api: {
    submit(form) {
      return new Promise((resolve, reject) => {
        form.validate((valid) => (valid ? resolve() : reject()));
      });
    },
    reset(form) {
      form.resetFields();
    },
  },
});

export const schema = [
  {
    name: 'name',
    label: '姓名',
    type: 'el-input',
    props: {
      placeholder: '请输入姓名',
      maxlength: 50,
      clearable: true,
      style: {
        width: '100%',
      },
    },
    rules: [{ required: true }],
    slots: {
      append: '.com',
    },
  },
  {
    name: 'age',
    label: '年龄',
    type: 'el-input',
    props: {
      style: {
        width: '100%',
      },
    },
    rules: { required: true },
  },
  {
    type: 'el-divider',
    props: {
      contentPosition: 'left',
    },
    slots: {
      default: '华丽的分割线',
    },
  },
  {
    label: '已婚',
    name: 'married',
    type: 'el-switch',
    rules: { required: true, message: '事件不能为空' },
    deps: {
      // 与年龄联动
      age: (value) => ({ active: value > 18, value: undefined }),
    },
  },
];

export default {
  components: { FormRender },
  computed: {
    schema() {
      return schema;
    },
  },
  methods: {
    submit() {
      this.$refs.form.submit().catch(console.log);
    },
  },
};
</script>
```

## License

MIT
